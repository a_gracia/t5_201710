package model.data_structures;

public interface ICola<T> {
	
	/**
	 * Inizializa la variable lista y listsize
	 */
	public void Queue();
	
	/**
	 * Agrega un elemento al inicio de la lista
	 */
	public void enqueue(T elemento);
	
	/**
	 * Retorna el primer elemento y lo elimina
	 */
	public T dequeue();
	
	/**
	 * Si listsize es 0 retorna true, de lo contrario retorna false
	 */
	public boolean isEmpty();
	
	/**
	 * Return el atributo listsize
	 */
	public int size();
	

}
