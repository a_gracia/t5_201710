package model.data_structures;

public class Pila<T> implements IPila<T> {

	int listsize;
	ILista<T> lista;
	
	public Pila(){
		listsize = 0;
		lista = new ListaEncadenada<T>();
		
	}
	@Override
	public void Stack() {
		listsize = 0;
		lista = new ListaEncadenada<T>();
	}

	@Override
	public void push(T elemento) {
		lista.agregarElementoFinal(elemento);
		listsize++;
	}

	@Override
	public T pop() {
		T a;
		if(listsize == 0){
			throw new IndexOutOfBoundsException();
		}
		else{
			T elemento = lista.darElementoT(listsize-1);
			lista.remove(listsize);
			listsize--;
			return elemento;
		}
	}

	@Override
	public boolean isEmpty() {
		return listsize == 0 ? true : false; 
	}

	@Override
	public int size() {
		return listsize;
	}
	

}
