package model.data_structures;

public interface IPila<T> {
	
	/**
	 * Inizializa la lista y el atributo listsize
	 */
	public void Stack();
	
	/**
	 * Agrega un elemento al tope de la pila
	 */
	public void push(T elemento);
	
	/**
	 * Retorna el ultimo elemento y lo elimina
	 */
	public T pop();
	
	/**
	 * Si listsize es 0 retorna true, de lo contrario retorna false
	 */
	public boolean isEmpty();
	
	/**
	 * Return el atributo listsize
	 */
	public int size();

	
}
