package model.data_structures;

public class Cola<T> implements ICola<T>{

	int listsize;
	ILista<T> lista;

	public Cola(){
		listsize =0;
		lista = new ListaEncadenada<T>();
	}

	@Override
	public void Queue() {
		listsize =0;
		lista = new ListaEncadenada<T>();
	}

	@Override
	public void enqueue(T elemento) {
		lista.agregarElementoFinal(elemento);
		listsize++;
	}

	@Override
	public T dequeue() {
		T elemento = lista.darElementoT(0);
		((ListaEncadenada<T>) lista).remove(0);
		listsize--;
		return elemento;
	}

	@Override
	public boolean isEmpty() {
		return listsize == 0 ? true : false; 
	}

	@Override
	public int size() {
		return listsize;
	}

}
