package model.data_structures;

import model.vo.VOPelicula;

public class MergeSort{

	private static void Merge(Comparable[]s, Comparable[] aux, int lo, int mid, int hi )
	{
		
		for(int i = lo; i <= hi; i++)
			aux[i] = s[i];
		
		int k = lo;
		int x = mid + 1; 
		for(int p = lo; p <= hi; p++)
		{
			if(k > mid)
			{
				s[p] = aux[x++];
			}
			else if(x > hi) 
			{
				s[p] = aux[k++];
			}
			else if(less(aux[x],aux[k])) 
			{
				s[p] = aux[x++]; 
			}
			else
			{
				s[p] = aux[k++];
			}
		}
	}
	
	private static void sort(Comparable[]a , Comparable[] aux, int lo, int hi)
	{
		if(hi <= lo) return;
		int mid = lo + (hi-lo)/2;
		sort(a,aux,lo, mid);
		sort(a, aux, mid+1, hi);
		Merge(a,aux, lo, mid, hi);
	}
	
	public static void Mergesort(Comparable[]a)
	{
		Comparable[] aux = new Comparable[a.length];
		sort(a, aux, 0, a.length - 1);
	}
	
	private static boolean less(Comparable v , Comparable w)
	{
		return v.compareTo(w) < 0;
	}
	
	
	private static void mergeLista(ListaEncadenada<Comparable>a, ListaEncadenada<Comparable>aux, int lo, int mid, int hi)
	{
		for(int i = 0; i < a.darNumeroElementos(); i++)
			aux.agregarElementoFinal(a.darElemento(i).elemento);
		
		
		int low = lo;
		int m = mid +1;
		for(int p = lo; p <= hi; p++)
		{
			if(low > mid)
			{
				a.darElemento(p).elemento = aux.darElemento(m++).elemento;
			}
			else if(m > hi) 
			{
				a.darElemento(p).elemento = aux.darElemento(low++).elemento; 
			}
			else if(less(aux.darElemento(m).elemento, aux.darElemento(low).elemento)) 
			{
				a.darElemento(p).elemento = aux.darElemento(m++).elemento;
			}
			else
			{
				a.darElemento(p).elemento = aux.darElemento(low++).elemento;
			}
		}
	}
	
	private static void auxSort(ListaEncadenada<Comparable> a , ListaEncadenada<Comparable> aux, int lo, int hi)
	{
		if(hi <= lo) return;
		int mid = lo + (hi-lo)/2;
		auxSort(a,aux,lo, mid);
		auxSort(a, aux, mid+1, hi);
		mergeLista(a,aux, lo, mid, hi);
	}
	
	public static void MergeSortLista(ListaEncadenada<Comparable> a)
	{
		ListaEncadenada<Comparable> aux = new ListaEncadenada<Comparable>();
		auxSort(a, aux, 0, a.darNumeroElementos()-1 );
	}

}
