package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaEncadenada.NodoSencillo;

public class ListaDobleEncadenada<T> implements ILista<T> {

	NodoDoble<T> primera = null;
	NodoDoble<T> actualactual=primera;
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterator<T> it = new MiIterator<T>(primera);
		return it;
	}
	 private class MiIterator <T> implements Iterator <T>  {

		NodoDoble<T> actual;

		public MiIterator (NodoDoble<T> PprimerElemento){
			actual = PprimerElemento; 
		}

		@Override
public boolean hasNext() {
			
			if(darNumeroElementos() == 0){
				return false;
			}
			
			if(actual == null)
				return true;
			return actual.siguiente != null;
		}

		@Override
		public T next() {
			if(actual == null)
				actual = (NodoDoble<T>) primera;
			else{
				actual = actual.siguiente;
			}
			return actual.elemento;
		}

	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoDoble<T> actual = primera;
		if(actual == null)
			primera = new NodoDoble<T>(elem);
	else{
			while(actual != null && actual.siguiente != null){

				actual = actual.siguiente;
			}
			actual.siguiente = new NodoDoble<T>(elem);
			actual.siguiente.anterior = actual;
		}

	}

	public NodoDoble<T> darElementoDoble(int pos) {

		NodoDoble<T> temp = primera;
		int cont = 0;
		while(temp.siguiente != null && cont <= pos-1 )
		{
			temp = temp.siguiente;
			cont++;
		}

		return temp;
	}
	
	public NodoSencillo<T> darElemento(int pos) {

		return null;
	}

	public T darElementoT(int pos) {

		NodoDoble<T> actual = primera;
		for(;pos > 0 && actual != null; pos--)
			actual = actual.siguiente;
		return actual == null ? null : actual.elemento; 
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		NodoDoble<T> actual = primera;
		int pos = 0;
		for(; actual != null; actual = actual.siguiente, pos++){};
		return pos;
	}

	public T remove(int posicion)
	{
		
		if(primera == null)
			throw new IndexOutOfBoundsException();

		else{
			NodoDoble<T> actual = primera;

			if(posicion == 0){
				
				T a = primera.elemento;
				primera.siguiente.anterior = null;
				primera = actual.siguiente;
				return a;
			}

			else
			{
				while(actual != null && posicion > 0)
				{
					actual = actual.siguiente;
					posicion --;					
				}

				if(posicion > 0)
					throw new IndexOutOfBoundsException();

				else
				{
					if (actual.siguiente != null)
					{
						T b = actual.elemento;
						actual.anterior.siguiente = actual.siguiente;
						actual.siguiente.anterior = actual.anterior;
						return b;
					}

					else
						actual.anterior.siguiente = null;
						return actual.elemento;

				}	
			}
		}	
	}
	
	public static class NodoDoble<T>
	{
		T elemento;
		NodoDoble<T> siguiente;
		NodoDoble<T> anterior;

		public NodoDoble(T elem) {
			this.elemento = elem;
		}
	}

	@Override
	public T darElementoFinal() {
		// TODO Auto-generated method stub
		return null;
	}

}
