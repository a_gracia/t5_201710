package Structures;


public class ColaPrioridadConCola <T extends Comparable<T>>{

 	private T[] pq;
 	private int n;
	
 	public void crearCP(int max) {
		pq = (T[]) new Comparable[max];
 	}

	public int darNumElementos() {
		return n;
	}

	
	public void agregar(T elemento) {
		pq[n++] = elemento;
	}

	
	public T max() {
		int max = 0;
		for (int i = 0; i < n; i++)
			if(less(max,i)) max =i;
		exch(max, n-1);
		return pq[--n];	
	}

	
	public boolean esVacia() {
		return n==0;
	}

	
	public int tamanoMax() {
		return n;
	}


	private boolean less(int max, int i) {
		return max < i;
	}
	private void exch(int max, int i) {
		max = i;
		i = max;
	}
	
}
