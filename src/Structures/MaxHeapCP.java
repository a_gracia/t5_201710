package Structures;

public class MaxHeapCP <T extends Comparable<T>>{

	int n=0;
	T[] p;


	public void crearCP(int max) {
		p = (T[]) new Comparable[max];
	}

	public int darNumElementos() {
		return n;
	}

	public void agregar(T elemento){
			p[++n] = elemento;
	}

	public T max() {
		T max = p [1];
		exch(1,n--);
		sink(1);
		p[n+1] = null;
		return max;

	}

	public boolean esVacia() {
		return n==0;
	}

	private void sink(int i) {
		while(2*i <= n){
			int j = 2*i;
			if(j < n && less(j,j+1)) j++;
			if(!less(i,j))break;
			exch(i,j);
			i = j;
		}
	}
	private void swin(int n2) {
		while(n2 > 1 && less(n2/2 , n2)){
			exch(n2, n2/2);
			n2 = n2/2;
		}
	}

	private boolean less(int max, int i) {
		return p[max].compareTo(p[i]) < 0;
	}
	private void exch(int max, int i) {
		T a = p[max];
		p[max] = p[i];
		p[i] = a;
	}
}
