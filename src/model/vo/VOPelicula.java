package model.vo;

import java.util.ArrayList;

import model.data_structures.ILista;

public class VOPelicula implements Comparable<VOPelicula>{
	
	private long idPelicula; 
	private String titulo;
	private int numeroRatings = 0;
	private int numeroTags = 0;
	private double promedioRatings = 0;
	private int agnoPublicacion = 0;
	private ILista<String> tagsAsociados;
	private ILista<String> generosAsociados;
	
	
	
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdUsuario(long idUsuario) {
		this.idPelicula = idUsuario;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() {
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	public int compareTo(VOPelicula comparar) {
		if(this.agnoPublicacion<comparar.getAgnoPublicacion())
			return -1;
		else if(this.agnoPublicacion>comparar.getAgnoPublicacion()) {
			return 1;
		}else{
			return this.titulo.compareTo(comparar.getTitulo());
		}
	}

}
