package model.data_structures;

import model.data_structures.ListaDobleEncadenada.NodoDoble;
import model.data_structures.ListaEncadenada.NodoSencillo;

public interface ILista<T> extends Iterable<T>{
	
	
	
	public void agregarElementoFinal(T elem);
	
	
	public T remove(int pos);
	
	public int darNumeroElementos();
	
	public NodoSencillo<T> darElemento(int pos);
	
	public T darElementoT(int p);


	public T darElementoFinal();
	
	

}
