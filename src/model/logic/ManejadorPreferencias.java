package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.MergeSort;
import model.data_structures.Pila;
import model.vo.VOGeneroPelicula;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;

public class ManejadorPreferencias {
	
	String allgeneros = "";
	private ILista<VOPelicula> misPeliculas;
	private ILista<VOGeneroPelicula> generos;
	ArrayList<Object> aux = new ArrayList<>();
	
	
	public boolean cargarArchivo(String rutaPeliculas) {

		misPeliculas = new ListaEncadenada<VOPelicula>();
		try {

			BufferedReader pelis = new BufferedReader(new FileReader(rutaPeliculas));
			String linea1 =pelis.readLine();
			long TInicio, TFin, tiempo;
			TInicio = System.currentTimeMillis();
			int contador = 0;
			generos = new ListaEncadenada<>();
			while((linea1 = pelis.readLine()) != null)
			{
				VOPelicula nuevaPelicula = new VOPelicula();
				String[] rompercomillas = linea1.split( "\"" );
				String[] auxiliargenero;
				ListaEncadenada<String> generol = new ListaEncadenada();
				
				if(rompercomillas.length == 3)
				{
					nuevaPelicula.setIdUsuario(Long.parseLong( rompercomillas[0].substring( 0, rompercomillas[0].length()-1 )));
					String[] romperparentecis = rompercomillas[1].split( "\\(" );
					if(romperparentecis.length != 1){
						nuevaPelicula.setAgnoPublicacion(Integer.parseInt(romperparentecis[romperparentecis.length-1].substring( 0, 4 )));
						nuevaPelicula.setTitulo(rompercomillas[1].substring(0, rompercomillas[1].length()-6));
					}else{
						nuevaPelicula.setTitulo(romperparentecis[0]);
					}
					auxiliargenero = (rompercomillas[rompercomillas.length-1].substring(1, rompercomillas[rompercomillas.length-1].length())).split("\\|");
					for (String i : auxiliargenero) {
						generol.agregarElementoFinal(i);
						if(allgeneros.indexOf(i)<0)
						{
							allgeneros += i + ",";
							VOGeneroPelicula individual = new VOGeneroPelicula();
							individual.setGenero(i);
							ILista<VOPelicula> f= new ListaEncadenada<>();
							f.agregarElementoFinal(nuevaPelicula);
							individual.setPeliculas(f);
							generos.agregarElementoFinal(individual);
						}else{
							for(int z = 0; z < generos.darNumeroElementos();z++){
								if(generos.darElementoT(z).getGenero().equals(i)){
									ILista<VOPelicula> f= new ListaEncadenada<>();
									f.agregarElementoFinal(nuevaPelicula);
									if(generos.darElementoT(z).getPeliculas()==null)
									{
										f.agregarElementoFinal(nuevaPelicula);
										generos.darElementoT(z).setPeliculas(f);
									}else
									{
										f=generos.darElementoT(z).getPeliculas();
										f.agregarElementoFinal(nuevaPelicula);
										generos.darElementoT(z).setPeliculas(f);
									}
								}
							}
						}
					}
				}
				else
				{
					String[] rompercomas = linea1.split(",");
					nuevaPelicula.setIdUsuario(Long.parseLong( rompercomas[0]));
					String[] romperparentecis = rompercomas[1].split( "\\(" );
					if(romperparentecis.length != 1){
						nuevaPelicula.setAgnoPublicacion(Integer.parseInt(romperparentecis[romperparentecis.length-1].substring( 0, 4 )));
						nuevaPelicula.setTitulo(rompercomas[1].substring(0, rompercomas[1].length()-6));
					}else{
						nuevaPelicula.setTitulo(romperparentecis[0]);
					}
					auxiliargenero = (rompercomas[rompercomas.length-1].substring(0, rompercomas[rompercomas.length-1].length())).split("\\|");
					for (String i : auxiliargenero) {
						generol.agregarElementoFinal(i);
						if(allgeneros.indexOf(i)<0)
						{
							allgeneros += i + ",";
							VOGeneroPelicula individual = new VOGeneroPelicula();
							individual.setGenero(i);
							ILista<VOPelicula> f= new ListaEncadenada<>();
							f.agregarElementoFinal(nuevaPelicula);
							individual.setPeliculas(f);
							generos.agregarElementoFinal(individual);
						}else{
							for(int z = 0; z < generos.darNumeroElementos();z++){
								if(generos.darElementoT(z).getGenero().equals(i)){
									ILista<VOPelicula> f= new ListaEncadenada<>();
									f.agregarElementoFinal(nuevaPelicula);
									if(generos.darElementoT(z).getPeliculas()==null)
									{
										f.agregarElementoFinal(nuevaPelicula);
										generos.darElementoT(z).setPeliculas(f);
									}else
									{
										f=generos.darElementoT(z).getPeliculas();
										f.agregarElementoFinal(nuevaPelicula);
										generos.darElementoT(z).setPeliculas(f);
									}
								}
							}
						}
					}
				}				
				nuevaPelicula.setGenerosAsociados((ILista<String>) generol);
				misPeliculas.agregarElementoFinal(nuevaPelicula);
			}
			pelis.close();
						
			allgeneros = allgeneros.substring(0,allgeneros.length()-1);
			String[] genros = allgeneros.split(",");
		
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
