package testColaPrioridad;
import java.util.Scanner;

import logic.GeneradorDatos;
import model.logic.ManejadorPreferencias;
import Structures.ColaPrioridad;
import Structures.MaxHeapCP;
import VO.PruebaVO;
import junit.framework.TestCase;

public class testColaPrioridad extends TestCase{
	
	PruebaVO[] prueba;
	MaxHeapCP<PruebaVO> heap = new MaxHeapCP<>();
	ColaPrioridad<PruebaVO> cola = new ColaPrioridad<>();
	int size =0;
 	GeneradorDatos generador = new GeneradorDatos();
	
	public void escenario1(){
	
 	prueba = new PruebaVO[3];
	PruebaVO a = new PruebaVO();
	a.setNombre("a");
	a.setAgno(new Integer(1900));
	prueba[0] = a;
	
	
	PruebaVO b = new PruebaVO();
	b.setNombre("b");
	b.setAgno(new Integer(1901));
	prueba[1] = b;
	
	PruebaVO c = new PruebaVO();
	c.setNombre("c");
	c.setAgno(new Integer(1902));
	prueba[2] = c;
	
	}
 	
 	public void escenario2Aleatorio(){
 		prueba = new PruebaVO[size];
 		
 		for(int i = 0; i< size ; i++){
 			PruebaVO nueva = new PruebaVO();
 			nueva.setAgno(generador.generarAgno(1)[0]);
 			nueva.setNombre(generador.generarCadenas(1)[0]);
 			prueba[i] = nueva;		
 		}
 	}
	
 	public void testMaxHeap() {
 		escenario1();
 		heap.crearCP(4);
 		cola.crearCP(4);
// 		
// 		heap.agregar(prueba[0]);
// 		heap.agregar(prueba[1]);
// 		heap.agregar(prueba[2]);
// 		
// 		cola.agregar(prueba[0]);
// 		cola.agregar(prueba[1]);
// 		cola.agregar(prueba[2]);
// 		
 		if(heap.max().getAgno() == 1900){
 			assertEquals(true, true);
 		}else{
 			assertEquals(true, false);
 		}
 	}
 	
 	public void testMaxCola() {
 		escenario1();
 		heap.crearCP(4);
 		cola.crearCP(4);
 		
 		heap.agregar(prueba[0]);
 		heap.agregar(prueba[1]);
 		heap.agregar(prueba[2]);
 		
 		cola.agregar(prueba[0]);
 		cola.agregar(prueba[1]);
 		cola.agregar(prueba[2]);
 		
 		if(cola.max().getAgno() == 1902){
 			assertEquals(true, true);
 		}else{
 			assertEquals(true, false);
 		}
 	}
 	
 	public void testAgregarHeap(){
 		escenario1();
 		heap.crearCP(4);
 		cola.crearCP(4);
 		
 		heap.agregar(prueba[0]);
 		heap.agregar(prueba[1]);
 		heap.agregar(prueba[2]);
 		
 		cola.agregar(prueba[0]);
 		cola.agregar(prueba[1]);
 		cola.agregar(prueba[2]);
 		
 		if(heap.darNumElementos() == 3){
 			assertEquals(true, true);
 		}else{
 			assertEquals(true, false);
 		}
 	}
 	
 	public void testAgregarCola() {
 		escenario1();
 		heap.crearCP(4);
 		cola.crearCP(4);
 		
 		heap.agregar(prueba[0]);
 		heap.agregar(prueba[1]);
 		heap.agregar(prueba[2]);
 		
 		cola.agregar(prueba[0]);
 		cola.agregar(prueba[1]);
 		cola.agregar(prueba[2]);
 		
 		if(cola.darNumElementos() == 3){
 			assertEquals(true, true);
 		}else{
 			assertEquals(true, false);
 		}
 	}
 	
 	
 	public void pruebasAgregarConValores(){
 		escenario2Aleatorio();
 		
 		System.out.println("Prueba para HeapMax");
 		heap.crearCP(size);
 		
 	}
 	public static void main(String[] args) {

		ManejadorPreferencias s = new ManejadorPreferencias();
		Scanner sc=new Scanner(System.in);

		boolean fin=false;


		while(!fin){
			printMenu();

			int option = sc.nextInt();
			switch(option){
			
			case 1:
				
				break;
			case 2:	
				fin=true;
				break;


			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 5----------------------");
		System.out.println("1. Numero de datos a generar para hacer las pruebas");

	}
}
