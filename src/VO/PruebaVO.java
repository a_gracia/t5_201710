package VO;

public class PruebaVO implements Comparable<PruebaVO> {

	String nombre;
	Integer agno;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getAgno() {
		return agno;
	}

	public void setAgno(Integer agno) {
		this.agno = agno;
	}

	@Override
	public int compareTo(PruebaVO a) {
		if(this.agno < a.agno) return -1;
		else if(this.agno > a.agno) return 1;
		else{
			if(this.nombre.compareTo(a.nombre) < 0) return -1;
			else if(this.nombre.compareTo(a.nombre)>0) return 1;
			else return 0;
		}
	}
}
