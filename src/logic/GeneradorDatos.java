package logic;
import java.util.Random;
public class GeneradorDatos {
	Random r = new Random();
	public String[] generarCadenas(int n){
		String[] res = new String[n];
		char[] letras = {'a','s','f','g','t','w','E','R','d','q','A','V','p'};
  		for (int i = 0; i < n; i++) {
			int a = r.nextInt(8000),  b = r.nextInt(letras.length-1);
			res[i] = ""+letras[b]+a;
		}return res;
	}
	public Integer[] generarAgno(int n){
		Integer[] res = new Integer[n];
		for (int i = 0; i < n; i++) {
			int a = r.nextInt(67);
			res[i] = new Integer(a+1950);
		}return res;
	}
}